(ns kit.aws.dynamodb.expression-builder-test
  (:require
   [clojure.test :refer :all]
   [kit.aws.dynamodb.expression-builder :as eb]))

(deftest clauses
  (is (= {:expression "#pk = :pk_1 AND #sk = :sk_1"
          :binding-vars {"#pk" "pk" "#sk" "sk"}
          :binding-vals {":pk_1" "index|name" ":sk_1" "foobar"}}
         (eb/with-new-expression-counter
           (eb/build-expression-anded-clauses {:pk "index|name" :sk "foobar"}))))
  (is (= {:expression "(#pk = :pk_1 AND #sk = :sk_1) OR (#pk = :pk_2 AND #sk = :sk_2)"
          :binding-vars {"#pk" "pk" "#sk" "sk"}
          :binding-vals {":pk_1" "index|name"
                         ":sk_1" "bar"
                         ":pk_2" "index|name"
                         ":sk_2" "foo"}}
         (eb/with-new-expression-counter
           (eb/build-expression-ored-clauses
            #{{:pk "index|name" :sk "foo"}
              {:pk "index|name" :sk "bar"}})))))

(deftest query
  (is (= {:key-condition-expression "#pk = :pk_1 AND #sk = :sk_1"
          :expression-attribute-names {"#pk" "pk", "#sk" "sk"}
          :expression-attribute-values {":pk_1" "index|name",
                                        ":sk_1" "Harry Potter"}}
         (eb/build-query {:pk "index|name" :sk "Harry Potter"})))

  (is (= {:key-condition-expression "(#pk = :pk_1 AND #sk = :sk_1) OR (#pk = :pk_2 AND #sk = :sk_2)"
          :expression-attribute-names {"#pk" "pk" "#sk" "sk"}
          :expression-attribute-values {":pk_1" "index|name"
                                        ":sk_1" "bar"
                                        ":pk_2" "index|name"
                                        ":sk_2" "foo"}}
         (eb/build-query
          #{{:pk "index|name" :sk "foo"}
            {:pk "index|name" :sk "bar"}})))
  (is (= {:key-condition-expression "#pk = :pk_1 AND begins_with(#sk, :sk_1)"
          :expression-attribute-names {"#pk" "pk" "#sk" "sk"}
          :expression-attribute-values {":pk_1" "index|name"
                                        ":sk_1" "foo"}}
         (eb/build-query
          {:pk "index|name"
           :sk {:operator :begins-with :value "foo"}})))
  (is (= {:key-condition-expression "#pk = :pk_1 AND #sk between :sk_1 and :sk_1_outer"
          :expression-attribute-names {"#pk" "pk" "#sk" "sk"}
          :expression-attribute-values {":pk_1" "index|age"
                                        ":sk_1" 18
                                        ":sk_1_outer" 40}}
         (eb/build-query
          {:pk "index|age"
           :sk {:operator :between :value1 18 :value2 40}}))))

(deftest update-item
  (is (= {:update-expression "SET #name = :name_1, #occupation = :occupation_1"
          :expression-attribute-names {"#name" "name"
                                       "#occupation" "occupation"}
          :expression-attribute-values {":name_1" "Harry James Potter"
                                        ":occupation_1" "Auror"}}
         (eb/build-update-item
          {:name "Harry James Potter"
           :occupation "Auror"})))

  (is (= {:update-expression "REMOVE #name, #occupation"
          :expression-attribute-names {"#name" "name"
                                       "#occupation" "occupation"}}
         (eb/build-update-item [:name :occupation]))))

(deftest case-test
  (is (= {:expression "#foo_name = :foo_name_1 AND #bar_name = :bar_name_1 AND #baz_name = :baz_name_1"
          :binding-vars {"#foo_name" "foo-name"
                         "#bar_name" "barName"
                         "#baz_name" "baz_name"}
          :binding-vals {":foo_name_1" "foo"
                         ":bar_name_1" "bar"
                         ":baz_name_1" "baz"}}
         (eb/with-new-expression-counter
           (eb/build-expression-anded-clauses
            {:foo-name "foo"
             :barName  "bar"
             :baz_name "baz"})))))
