(ns kit.aws.dynamodb-test
  (:require
   [clojure.test :refer :all]
   [cognitect.aws.credentials :as creds]
   [kit.util :as u]
   [kit.log :as log]
   [kit.aws.dynamodb :as ddb]
   [kit.aws.dynamodb.expression-builder :as ddbeb]))

(def tname "ddb-testdb")
(defn fixture [f]
  (log/log-errors
   (ddb/init!
    {:region "us-west-2"
     :credentials-provider (creds/basic-credentials-provider
                            {:access-key-id "dummy"
                             :secret-access-key "dummy"})
     :endpoint-override {:hostname (or (not-empty
                                        (System/getenv "DYNAMODB_LOCAL_HOST"))
                                       "localhost")
                         :port 8000
                         :protocol :http}})
   (u/ignore-errors
    (ddb/delete-table {:table-name tname}))
   (ddb/create-table
    {:table-name tname
     :attribute-definitions [{:AttributeName "pk"
                              :AttributeType "S"}
                             {:AttributeName "sk"
                              :AttributeType "S"}]
     :key-schema [{:AttributeName "pk"
                   :KeyType "HASH"}
                  {:AttributeName "sk"
                   :KeyType "RANGE"}]
     :billing-mode "PAY_PER_REQUEST"})
   (f)))

(use-fixtures :each #'fixture)

(deftest conn-test
  (is (contains? (set (:table-names (ddb/list-tables))) tname)))

(deftest put-get-item
  (is (ddb/put-item
       {:table-name tname
        :item {:pk "person" :sk "001" :name "Paul"}}))
  (is (= {:item {:pk "person" :sk "001" :name "Paul"}}
         (ddb/get-item
          {:table-name tname
           :key {:pk "person" :sk "001"}}))))

(deftest update-item
  (is (ddb/put-item
       {:table-name tname
        :item {:pk "person" :sk "003" :name "Duncan" :role "Swordmaster"}}))
  (is (ddb/update-item
       (merge {:table-name tname
               :key {:pk "person" :sk "003"}}
              (ddbeb/build-update-item
               {:name "Hayt"}))))
  (is (= {:item {:pk "person" :sk "003" :name "Hayt" :role "Swordmaster"}}
         (ddb/get-item
          {:table-name tname
           :key {:pk "person" :sk "003"}})))
  (is (ddb/update-item
       (merge {:table-name tname
               :key {:pk "person" :sk "003"}}
              (ddbeb/build-update-item [:role]))))
  (is (= {:item {:pk "person" :sk "003" :name "Hayt"}}
         (ddb/get-item
          {:table-name tname
           :key {:pk "person" :sk "003"}}))))

(deftest batch-test
  (is (= {:unprocessed-items {}
          :consumed-capacity [{:table-name tname :capacity-units 10.0}]}
         (ddb/batch-write-item
          {:request-items
           {tname (map (fn [x] {:put-request {:item x}})
                       [{:pk "person" :sk "001" :name "Harry"}
                        {:pk "person" :sk "002" :name "Hermione"}
                        {:pk "person" :sk "003" :name "Ron"}
                        {:pk "person" :sk "004" :name "Albus"}
                        {:pk "person" :sk "005" :name "Severus"}
                        {:pk "person" :sk "006" :name "Minerva"}
                        {:pk "person" :sk "007" :name "Rubeus"}
                        {:pk "person" :sk "008" :name "Tom"}
                        {:pk "person" :sk "009" :name "Sirius"}
                        {:pk "person" :sk "010" :name "Draco"}])}
           :return-consumed-capacity "TOTAL"})))
  (is (= {:item {:pk "person" :sk "003" :name "Ron"}}
         (ddb/get-item
          {:table-name tname
           :key {:pk "person" :sk "003"}})))
  (is (= 10
         (-> (ddb/batch-get-item
              {:request-items
               {tname {:keys (map (fn [x]
                                    {:pk "person"
                                     :sk (format "%03d" (inc x))})
                                  (range 10))}}})
             :responses
             (get (keyword tname))
             (count))))
  (is (= "person"
         (-> (ddb/batch-get-item
              {:request-items
               {tname {:keys (map (fn [x]
                                    {:pk "person"
                                     :sk (format "%03d" (inc x))})
                                  (range 10))}}})
             :responses
             (get-in [(keyword tname)])
             (first)
             (get :pk)))))

(deftest transact-write-items
  (is (ddb/transact-write-items
       {:transact-items
        [{:put {:table-name tname :item {:pk "person" :sk "002" :name "Chani"}}}
         {:delete {:table-name tname :key {:pk "person" :sk "001"}}}]}))
  (is (= {:item {:pk "person" :sk "002" :name "Chani"}}
         (ddb/get-item {:table-name tname
                        :key {:pk "person" :sk "002"}})))
  (is (nil? (ddb/get-item {:table-name tname
                           :key {:pk "person" :sk "001"}})))
  (is (= {:consumed-capacity
	  [{:table-name tname
	    :capacity-units 6.0
	    :write-capacity-units 6.0}]}
         (ddb/transact-write-items
          {:transact-items
           [{:put {:table-name tname :item {:pk "person" :sk "002" :name "Chani"}}}
            {:delete {:table-name tname :key {:pk "person" :sk "001"}}}]
           :return-consumed-capacity "TOTAL"}))))

(def test-db
  {nil      {:items [{:name "Paul"}
                     {:name "Chani"}
                     {:name "Leto"}
                     {:name "Gurney"}]
             :last-evaluated-key "token1"}
   "token1" {:items [{:name "Baron"}
                     {:name "Piter"}
                     {:name "Rabban"}
                     {:name "Feyd"}]
             :last-evaluated-key "token2"}
   "token2" {:items [{:name "Stilgar"}
                     {:name "Jamis"}
                     {:name "Liet"}
                     {:name "Korba"}]}})

(deftest mock-scan-seq-test
  (with-redefs [ddb/scan (fn [{:keys [limit exclusive-start-key]}]
                           (get test-db exclusive-start-key))]
    (is (=  8 (count (ddb/scan-seq {:limit 8}))))
    (is (= 12 (count (ddb/scan-seq {})))))
  (with-redefs [ddb/scan (constantly nil)]
    (is (= 0 (count (ddb/scan-seq {:limit 4}))))
    (is (= 0 (count (ddb/scan-seq {}))))))

(deftest mock-query-seq-test
  (with-redefs [ddb/query (fn [{:keys [limit exclusive-start-key]}]
                            (get test-db exclusive-start-key))]
    (is (=  4 (count (ddb/query-seq {:limit 4}))))
    (is (= 12 (count (ddb/query-seq {})))))
  (with-redefs [ddb/query (constantly nil)]
    (is (= 0 (count (ddb/query-seq {:limit 4}))))
    (is (= 0 (count (ddb/query-seq {}))))))
