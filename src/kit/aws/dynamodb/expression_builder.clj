(ns kit.aws.dynamodb.expression-builder
  (:require
   [clojure.string :as str]
   [camel-snake-kebab.core :as csk]))

(defn as-expression-value [v]
  (if (map? v)
    v
    {:operator := :value v}))

(def ^:dynamic *expression-counter* (atom {}))
(defmacro with-new-expression-counter [& body]
  `(binding [*expression-counter* (atom {})]
     ~@body))

(defn expression-counter
  ([]
   (expression-counter :global))
  ([key]
   (get (swap! *expression-counter* update key (fnil inc 0)) key)))

(defn make-expression-attribute-aliases [k]
  (let [kstr (csk/->snake_case_string k)]
    [(format "#%s" kstr)
     (format ":%s_%d" kstr (expression-counter kstr))
     kstr]))

(defmulti build-expression-clause (fn [k {:keys [operator]}] operator))

(defmethod build-expression-clause :begins-with [k v]
  (let [{:keys [value]} v
        [bvar bval] (make-expression-attribute-aliases k)]
    {:expression (format "begins_with(%s, %s)" bvar bval)
     :binding-vars {bvar (name k)}
     :binding-vals {bval value}}))

(defmethod build-expression-clause :between [k v]
  (let [{:keys [value1 value2]} v
        [bvar bval] (make-expression-attribute-aliases k)
        bval2 (format "%s_outer" bval)]
    {:expression (format "%s between %s and %s" bvar bval bval2)
     :binding-vars {bvar (name k)}
     :binding-vals {bval value1
                    bval2 value2}}))

(defmethod build-expression-clause :default [k v]
  (let [{:keys [operator value]} v
        [bvar bval] (make-expression-attribute-aliases k)]
    {:expression (format "%s %s %s"
                         bvar
                         (name operator)
                         bval)
     :binding-vars {bvar (name k)}
     :binding-vals {bval value}}))

(defn build-expression-anded-clauses
  ([expr]
   (build-expression-anded-clauses expr "AND"))
  ([expr operator]
   (let [clauses (map (fn [[k v]]
                        (build-expression-clause k (as-expression-value v)))
                      expr)
         expression (str/join (str " " operator " ")
                              (map :expression clauses))
         bvars (reduce merge {} (map :binding-vars clauses))
         bvals (reduce merge {} (map :binding-vals clauses))]
     {:expression expression
      :binding-vars bvars
      :binding-vals bvals})))

(declare build-expression)

(defn build-expression-ored-clauses [exprs]
  (reduce (fn [m expr]
            (let [clause (build-expression expr)]
              (-> m
                  (update :expression #(if (nil? %)
                                         (str "(" (:expression clause) ")")
                                         (str % " OR ("
                                              (:expression clause)
                                              ")")))
                  (update :binding-vars merge (:binding-vars clause))
                  (update :binding-vals merge (:binding-vals clause)))))
          {}
          exprs))

(defn build-expression [expr]
  (cond
    (map? expr) (build-expression-anded-clauses expr)
    (coll? expr) (build-expression-ored-clauses expr)))

(defn build-query
  "Takes a map or set (of maps) as 'expr', and generates a dynamodb
  query expression, and its binding variables and values. e.g.,

  (build-query {:pk \"index|name\" :sk \"Harry Potter\"})
  => {:key-condition-expression \"#pk = :pk_1 AND #sk := :sk_1\"
      :expression-attribute-names {\"#pk\" \"pk\", \"#sk\" \"sk\"}
      :expression-attribute-values {\":pk_1\" {:S \"index|name\"},
                                    \":sk_1\" {:S \"Harry Potter\"}}}"
  [expr]
  (let [{:keys [expression
                binding-vars
                binding-vals]} (with-new-expression-counter
                                 (build-expression expr))]
    {:key-condition-expression expression
     :expression-attribute-names binding-vars
     :expression-attribute-values binding-vals}))

(defn build-update-item-set-operation [expr]
  (reduce (fn [a [k v]]
            (let [[expr attrib-vars attrib-vals] a
                  [bvar bval] (make-expression-attribute-aliases k)]
              [(str expr
                    (if (str/blank? expr) "" ", ")
                    bvar " = " bval)
               (assoc attrib-vars bvar (name k))
               (assoc attrib-vals bval v)]))
          ["" {} {}]
          expr))

(defn build-update-item-remove-operation [expr]
  (reduce (fn [a k]
            (let [[expr attrib-vars] a
                  [bvar bval] (make-expression-attribute-aliases k)]
              [(str expr
                    (if (str/blank? expr) "" ", ")
                    bvar)
               (assoc attrib-vars bvar (name k))]))
          ["" {}]
          expr))

(defn build-update-item
  ([expr]
   (cond
     (map? expr) (build-update-item :set expr)
     (coll? expr) (build-update-item :remove expr)))
  ([op expr]
   (case op
     :set
     (with-new-expression-counter
       (let [[update-expr attrib-vars attrib-vals] (build-update-item-set-operation expr)]
         {:update-expression (str "SET " update-expr)
          :expression-attribute-names attrib-vars
          :expression-attribute-values attrib-vals}))

     :remove
     (with-new-expression-counter
       (let [[update-expr attrib-vars attrib-vals] (build-update-item-remove-operation expr)]
         {:update-expression (str "REMOVE " update-expr)
          :expression-attribute-names attrib-vars})))))
