(ns kit.aws.dynamodb.codec
  (:require
   [clojure.string :as str]
   [kit.util :as u]))

(declare decode-value)

(defn decode-map [m]
  (reduce (fn [m1 [k v]] (assoc m1 k (decode-value v))) {} m))

(defn encoded-string? [s]
  (.startsWith s "#clj "))

(defn decode-string [s]
  (if (encoded-string? s)
    (u/read-string-safely (apply str (drop 5 s)))
    s))

(defn decode-value [m]
  (let [[type value] (first m)]
    (case (keyword (str/upper-case (name type)))
      :BOOL value
      :NULL nil
      :B    (u/base64-decode value)
      :N    (u/read-string-safely value)
      :S    (decode-string value)
      :NS   (set value)
      :SS   (set (map decode-string value))
      :L    (mapv decode-value value)
      :M    (decode-map value))))

(defn decode [obj]
  (if (map? obj)
    (decode-map obj)
    (mapv decode obj)))

(declare encode-value)

(defn encode-map [m]
  (reduce (fn [m1 [k v]]
            (assoc m1 k (encode-value v)))
          {}
          m))

(defn encode-string [s]
  (if (encoded-string? s)
    (str "#clj " (pr-str s))
    s))

(def ^:dynamic *encode-readably* true)

(defn encode-readable-str [value]
  (if *encode-readably*
    (str "#clj " (pr-str value))
    value))

(defn encode-value [value]
  (cond
    (nil? value) {:NULL true}
    (boolean? value) {:BOOL value}
    (number? value) {:N (str value)}
    (string? value) {:S (encode-string value)}
    (symbol? value) {:S (encode-readable-str value)}
    (keyword? value) {:S (encode-readable-str value)}
    (and (set? value) (every? number? value)) {:NS (vec value)}
    (set? value) {:SS (mapv encode-readable-str value)}
    (sequential? value) {:L (mapv encode-value value)}
    (map? value) {:M (encode-map value)}))

(defn encode [obj]
  (if (map? obj)
    (encode-map obj)
    (mapv encode obj)))
