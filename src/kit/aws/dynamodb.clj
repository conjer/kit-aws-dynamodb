(ns kit.aws.dynamodb
  (:require
   [cognitect.anomalies :as ca]
   [camel-snake-kebab.core :as csk]
   [camel-snake-kebab.extras :as cske]
   [kit.util :as u]
   [kit.aws.core :as ac]
   [kit.aws.dynamodb.codec :as codec]))

(defonce client (atom nil))

(defn init!
  "Initialize the dynamodb client by calling
  `cognitect.aws.client.api/client`, with the optional 'conf' map."
  ([]
   (init! {}))
  ([conf]
   (reset! client (ac/make-client
                   (merge {:api :dynamodb} conf)))))

;; (reset! client nil)
;; (init! {})

(defn map->PascalCase
  ([m]
   (map->PascalCase m false))
  ([m shallow?]
   ((if shallow?
      u/shallow-transform-keys
      cske/transform-keys) csk/->PascalCaseKeyword m)))

(defn map->kebab-case [m]
  (cske/transform-keys csk/->kebab-case-keyword m))

(defn list-tables
  "Return the list of tables present and accessible in dynamodb"
  []
  (ac/invoke @client :list-tables))

;; (list-tables)

(defn update-if-contains? [m k f & args]
  (if (contains? m k)
    (apply update m k f args)
    m))

(defn update-with-codec [codec object ks]
  (reduce (fn [object k]
            (update-if-contains? object k codec))
          object
          ks))

(defn invoke-request
  ([op request]
   (invoke-request op request {}))
  ([op request {:keys [encode-attribs decode-attribs]}]
   (let [request (update-with-codec codec/encode
                                    request
                                    (concat [:expression-attribute-values]
                                            encode-attribs))]
     (update-with-codec codec/decode
                        (ac/invoke @client op request)
                        (concat [:items] decode-attribs)))))

(defn delete-table
  "Delete a table, e.g.,

  (delete-table {:table-name \"table-name\"})"
  [request]
  (invoke-request :delete-table request))

(defn create-table
  "Create a table, e.g.,

   (create-table
    {:table-name \"table-name\"
     :attribute-definitions [{:AttributeName \"pk\"
                              :AttributeType \"S\"}
                             {:AttributeName \"sk\"
                              :AttributeType \"S\"}]
     :key-schema [{:AttributeName \"pk\"
                   :KeyType \"HASH\"}
                  {:AttributeName \"sk\"
                   :KeyType \"RANGE\"}]
     :billing-mode \"PAY_PER_REQUEST\"})"
  [request]
  (invoke-request :create-table request))

(defn scan-query-basic [f {:as request :keys [limit]} collection-key]
  (let [item-count (atom 0)]
    (u/scrolling-seq
     (fn [start-key]
       (let [result (f
                     (merge request
                            (when start-key
                              {:exclusive-start-key start-key})))
             {:keys [last-evaluated-key]} result
             items (u/as-coll (collection-key result))]
         (swap! item-count + (count items))
         [items
          (and (or (not limit) (< @item-count limit))
               (not-empty last-evaluated-key))])))))

(defn scan
  "Invoke the dynamodb Scan API with the 'request' map, e.g.,

  (scan
   {:table-name \"users\"
    :filter-expression \"attribute_not_exists(user_id)\"
    :select \"COUNT\"})"
  [request]
  (invoke-request :scan request))

(defn scan-seq-basic [request collection-key]
  (scan-query-basic scan request collection-key))

(defn scan-seq
  "Creates a seq abstraction on top of the basic `scan` function,
  returns a lazy-seq, which encapsulates paging through multiple Scan
  API requests."
  [request]
  (scan-seq-basic request :items))

(defn scan-seq-count [request]
  (reduce + 0 (scan-seq-basic request :count)))

(defn query
  "Invoke the dynamodb Query API with 'request' map, e.g.,

  (query
   {:table-name \"users\"
    :key-condition-expression \"id = :id\"
    :expression-attribute-values {\":id\" \"123\"}})"
  [request]
  (invoke-request :query request))

(defn query-seq-basic [request collection-key]
  (scan-query-basic query request collection-key))

(defn query-seq
  "Creates a seq abstraction on top of the basic `query` function,
  returns a lazy-seq, which encapsulates paging through multiple Query
  API requests."
  [request]
  (query-seq-basic request :items))

(defn query-seq-count [request]
  (reduce +
          0
          (query-seq-basic (merge request
                                  {:select "COUNT"})
                           :count)))

(defn get-item
  "Invoke the dynamodb GetItem API request, e.g.,

  (get-item {:table-name \"user\"
             :key {:pk \"user-id\"
                   :sk \"123\"}})"
  [request]
  (not-empty
   (invoke-request :get-item
                   request
                   {:encode-attribs [:key]
                    :decode-attribs [:item]})))

(defn put-item
  "Invoke the dynamodb PutItem API request, e.g.,

  (put-item
   {:table-name \"user\"
    :item {:pk \"user-id\"
           :sk \"123\"
           :name \"test-user\"
           :id \"123\"
           :role \"admin\"}})"
  [request]
  (invoke-request :put-item
                  request
                  {:encode-attribs [:item]}))

(defn delete-item
  "Invoke the dynamodb DeleteItem API request, e.g.,

  (delete-item {:table-name \"user\"
                :key {:pk \"user-id\" :sk \"123\"}})"
  [request]
  (invoke-request :delete-item request {:encode-attribs [:key]}))

(defn transact-write-items
  "Invoke the dynamodb TransactWriteItems API request, e.g.,

  (transact-write-items
   {:transact-items
    [{:put {:table-name \"table-name\"
            :item {:id \"ident-1\" :name \"test-name\"}}}
     {:delete {:table-name :testTable
               :key {:id \"ident-2\"}}}]})"
  [request]
  (let [encode-attrs #(update-with-codec codec/encode % [:Item :Key])
        encode-transact-items (fn [transact-items]
                                (map (fn [m]
                                       (->> m
                                            (map (fn [[k v]]
                                                   [(csk/->PascalCaseKeyword k)
                                                    (-> v
                                                        (map->PascalCase true)
                                                        (encode-attrs))]))
                                            (into {})))
                                     transact-items))
        encode (fn [request]
                 (update request :transact-items encode-transact-items))
        decode (fn [response]
                 (map->kebab-case response))]
    (->> (encode request)
         (invoke-request :transact-write-items)
         (decode))))

(defn batch-write-item
  "Invoke the dynamodb BatchWriteItem API request, e.g.,

  (batch-write-item
   {:request-items
    {\"table\" [{:put-request {:item {:id \"ident-1\" :name \"test-name\"}}}
                {:delete-request {:key {:id \"ident-2\"}}}]}})"
  [request]
  (let [encode-attrs (fn [x]
                       (update-with-codec codec/encode x [:Item :Key]))
        encode-request-item (fn [[table-name requests]]
                              [table-name
                               (map (fn [m]
                                      (->> m
                                           (map
                                            (fn [[k v]]
                                              [(csk/->PascalCaseKeyword k)
                                               (-> v
                                                   (map->PascalCase true)
                                                   (encode-attrs))]))
                                           (into {})))
                                    requests)])
        encode (fn [request]
                 (update request
                         :request-items
                         (fn [request-items]
                           (->> request-items
                                (map encode-request-item)
                                (into {})))))
        decode (fn [response]
                 (map->kebab-case response))]
    (->> (encode request)
         (invoke-request :batch-write-item)
         (decode))))

(defn update-item
  "Invoke the UpdateItem API request, e.g.,

  (ddb/update-item
   (merge {:table-name \"table-name\"
           :key {:pk \"person\" :sk \"007\"}}
          (expression-builder/build-update-item
           {:name \"Bond, James\"})))"
  [request]
  (invoke-request :update-item
                  request
                  {:encode-attribs [:key]}))

(defn batch-get-item
  "Invoke the BatchGetItem API request, e.g.,

  (ddb/batch-get-item
   {:request-items
    {\"table-name\" {:keys [{:pk \"person\" :sk \"007\"}
                            {:pk \"person\" :sk \"008\"}]}}})
  => [{:pk \"person\" :sk \"007\" :name \"James Bond\"}
      {:pk \"person\" :sk \"008\" :name \"A different Double-O\"}]"
  [request]
  (let [encode-keys (fn [request]
                      (update-with-codec codec/encode
                                         (map->PascalCase request true)
                                         [:Keys]))
        encode (fn [request]
                 (update request
                         :request-items
                         (fn [request-items]
                           (reduce (fn [m [table-name request]]
                                     (assoc m
                                            table-name
                                            (encode-keys request)))
                                   {}
                                   request-items))))
        decode (fn [response]
                 (update response
                         :responses
                         #(->> %
                               (map (fn [[table-name responses]]
                                      [table-name
                                       (map codec/decode responses)]))
                               (into {}))))]
    (->> (encode request)
         (invoke-request :batch-get-item)
         (decode))))
